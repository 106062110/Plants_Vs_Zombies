
var loadingState={
    preload: function(){
        game.load.image('background','assets/menu/logo.jpg');
        game.load.image('logoword','assets/menu/LogoWord.jpg');
        game.load.image('loadbar','assets/menu/LoadBar.png');
        game.load.image('teamlogo','assets/menu/team_logo.png');
        game.load.audio('menu_music', 'assets/music/menu_music.mp3');
    },
    create: function(){
        game.stage.backgroundColor = '#ffffff';
        game.add.sprite(0, 0, 'background');
        game.add.sprite(420, 20, 'logoword');
        this.teamlogo = game.add.sprite(100, 530, 'teamlogo');
        this.teamlogo.anchor.set(0.5);
        this.teamlogo.scale.setTo(0.6,0.6);


        this.loadbar = game.add.sprite(470, 500, 'loadbar');
        this.loadbar.anchor.set(0.5);
        this.loadbar.inputEnabled =true;
        this.loadbar.input.useHandCursor = true;


        this.loadbar.events.onInputDown.add(function(){
            //music.destroy();
            game.state.start('menu');
        }, this);

        music = game.add.audio('menu_music');
        music.loop = true;
        if(!muteflag) music.play();


    },
    update: function(){

    },


}
