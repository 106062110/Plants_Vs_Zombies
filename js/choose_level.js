var sss=1;
// var pass_level1 = true;
// var pass_level2 = false;
var pass_level3 = false;
var choose_levelState={
    preload: function(){
        game.load.image('level','assets/choose_level/level.png');

        game.load.image('level1','assets/choose_level/level1.png');
        game.load.image('level1_check','assets/choose_level/level1_check.png');
        game.load.image('level2','assets/choose_level/level2.png');
        game.load.image('level2_check','assets/choose_level/level2_check.png');
        game.load.image('level2_black','assets/choose_level/level2_black.png');
        game.load.image('level3','assets/choose_level/level3.png');
        game.load.image('level3_check','assets/choose_level/level3_check.png');
        game.load.image('level3_black','assets/choose_level/level3_black.png');

    },
    create: function(){
        console.log("into choose_level");
        game.stage.backgroundColor = '#ffffff';

        game.add.sprite(0, 0, 'level');


        if(pass_level1)  level1 = game.add.sprite(200, 180, 'level1_check');
        else level1 = game.add.sprite(200, 180, 'level1');
        level1.anchor.setTo(0.5);
        level1.scale.setTo(0.5, 0.5);
        level1.inputEnabled = true;

        if(pass_level2)  level2 = game.add.sprite(500, 180, 'level2_check');
        else if(!pass_level2 && pass_level1)     level2 = level2 = game.add.sprite(500, 180, 'level2');
        else if(!pass_level2 && !pass_level1)    level2 = level2 = game.add.sprite(500, 180, 'level2_black');
        level2.anchor.setTo(0.5);
        level2.scale.setTo(0.5, 0.5);
        level2.inputEnabled = true;

        if(pass_level3)  level3 = game.add.sprite(800, 180, 'level3_check');
        else if(!pass_level3 && pass_level2)     level3 = game.add.sprite(800, 180, 'level3');
        else if(!pass_level3 && !pass_level2)    level3 = game.add.sprite(800, 180, 'level3_black');
        level3.anchor.setTo(0.5);
        level3.scale.setTo(0.5, 0.5);
        level3.inputEnabled = true;

        circle = new Phaser.Circle(49,540,72);//x,y,diameter
    },
    update: function(){
        x = game.input.activePointer.positionDown.x;//點下去的位置
        y = game.input.activePointer.positionDown.y;

        level1.events.onInputDown.add(function(){
            level1.scale.setTo(0.9);
            if(level2.scale.x===0.9) level2.scale.setTo(0.5);
            if(level3.scale.x===0.9) level3.scale.setTo(0.5);
            this.level_flag = 1;
        }, this);

        if(pass_level1){
            level2.events.onInputDown.add(function(){
                level2.scale.setTo(0.9);
                if(level1.scale.x===0.9) level1.scale.setTo(0.5);
                if(level3.scale.x===0.9) level3.scale.setTo(0.5);
                this.level_flag = 2;
            }, this);
        }

        if(pass_level1 && pass_level2){
            level3.events.onInputDown.add(function(){
                level3.scale.setTo(0.9);
                if(level2.scale.x===0.9) level2.scale.setTo(0.5);
                if(level1.scale.x===0.9) level1.scale.setTo(0.5);
                this.level_flag = 3;
            }, this);
        }
        //console.log('x:'+x,'y:'+y);

        switch(this.level_flag){
            case 1:
                if(136<x && x<263 && 290<y && y<325) game.state.start('level_1');
                break;
            case 2:
                if(435<x && x<563 && 290<y && y<325) game.state.start('level_2');
                break;
            case 3:
                if(735<x && x<863 && 290<y && y<325) game.state.start('menu');
                break;
            default:
                console.log('error');
                break;
        }

        if(circle.contains(x,y)) game.state.start('menu');

    },


}
