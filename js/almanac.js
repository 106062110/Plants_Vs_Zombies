var almanacState={
    preload: function(){
        game.load.image('background','assets/almanac/Almanac.jpg');

        game.load.spritesheet('sunflower','assets/almanac/sunflower_s.png',73,74);
        game.load.spritesheet('zombie_s','assets/almanac/zombie_s.png',166,144);
        game.load.spritesheet('check_plant','assets/almanac/check_plant.png',170,62);
        game.load.spritesheet('check_zombie','assets/almanac/check_zombie.png',167,57);
        game.load.spritesheet('Almanac_CloseButton','assets/almanac/Almanac_CloseButton.png',89,26);
    },
    create: function(){
        game.add.sprite(0, 0, 'background');
        this.sunflower= game.add.sprite(220, 240, 'sunflower');
        this.sunflower.animations.add('swing2',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],0,false);
        this.sunflower.play('swing2',12, true, true);

        this.zombie= game.add.sprite(630, 180, 'zombie_s');
        this.zombie.animations.add('walk',[0,1,2,3,4,5,6,7,8,9,10],0,false);
        this.zombie.play('walk',7, true, true);

        //this.check_plant = game.add.sprite(170, 330, 'check_plant');
        //this.check_zombie = game.add.sprite(660, 330, 'check_zombie');
        game.add.button(170,330, 'check_plant', function(){ game.state.start('plantpage');},this, 1, 0,1,0)//over out down up
        game.add.button(660,330, 'check_zombie', function(){game.state.start('zombiepage');},this, 1, 0,1,0)//over out down up
        game.add.button(850,567,'Almanac_CloseButton',function(){game.state.start('menu');},this, 1, 0,1,0)//over out down up
    },
    update: function(){


    },


}


var plantpageState={
    preload: function(){
        game.load.image('background','assets/almanac/Almanac_Plant.jpg');
        game.load.spritesheet('Almanac_CloseButton','assets/almanac/Almanac_CloseButton.png',89,26);
        game.load.spritesheet('Almanac_IndexButton','assets/almanac/Almanac_IndexButton.png',164,26);
        game.load.image('plant1','assets/almanac/plant1.png');
        game.load.image('plant2','assets/almanac/plant2.png');
        game.load.image('plant3','assets/almanac/plant3.png');
        game.load.image('plant4','assets/almanac/plant4.png');
        game.load.image('plant2_black','assets/almanac/plant2_black.png');
        game.load.image('plant2_black_check','assets/almanac/plant2_black_check.png');
        game.load.image('plant3_black','assets/almanac/plant3_black.png');
        game.load.image('plant3_black_check','assets/almanac/plant3_black_check.png');
        game.load.image('plant4_black','assets/almanac/plant4_black.png');
        game.load.spritesheet('Peashooter_s','assets/almanac/Peashooter_s.png',71,71);
        game.load.spritesheet('sunflower_s','assets/almanac/sunflower_s.png',73,74);
        game.load.spritesheet('wallnut_s','assets/almanac/wallnut_s.png',65,73);
        game.load.spritesheet('CherryBomb_s','assets/almanac/CherryBomb_s.png',112,81);
        game.load.image('plant1_card','assets/almanac/plant1_card.png');
        game.load.image('plant2_card','assets/almanac/plant2_card.png');
        game.load.image('plant3_card','assets/almanac/plant3_card.png');
        game.load.image('plant4_card','assets/almanac/plant4_card.png');
    },
    create: function(){
        game.add.sprite(0, 0, 'background');
        game.add.button(850,567,'Almanac_CloseButton',function(){game.state.start('menu');},this, 1, 0,1,0)//over out down up
        game.add.button(60,567,'Almanac_IndexButton',function(){game.state.start('almanac');},this, 1, 0,1,0)//over out down up
        this.plant1_card = game.add.sprite(610, 90, 'plant1_card');
        this.plant1_card.visible = false;
        this.plant2_card = game.add.sprite(610, 90, 'plant2_card');
        this.plant2_card.visible = false;
        this.plant3_card = game.add.sprite(610, 90, 'plant3_card');
        this.plant3_card.visible = false;
        this.plant4_card = game.add.sprite(610, 90, 'plant4_card');
        this.plant4_card.visible = false;
        this.plant1 = game.add.sprite(40, 100, 'plant1');
        if(pass_level1) this.plant2 = game.add.sprite(150, 100, 'plant2');
        else this.plant2 = game.add.sprite(150, 100, 'plant2_black');
        if(pass_level1) this.plant3 = game.add.sprite(260, 100, 'plant3');
        else this.plant3 = game.add.sprite(260, 100, 'plant3_black');
        if(pass_level2) this.plant4 = game.add.sprite(370, 100, 'plant4');
        else this.plant4 = game.add.sprite(370, 100, 'plant4_black');
        this.plant1.inputEnabled =true;
        this.plant2.inputEnabled =true;
        this.plant3.inputEnabled =true;
        this.plant4.inputEnabled =true;
        this.plant1.events.onInputDown.add(function(){
            this.peashooter.visible = true;
            this.sunflower.visible = false;
            this.wallnut.visible = false;
            this.cherrybomb.visible = false;
            this.plant1_card.visible = true;
            this.plant2_card.visible = false;
            this.plant3_card.visible = false;
            this.plant4_card.visible = false;
        }, this);
        this.plant2.events.onInputDown.add(function(){
            if(pass_level1){
                this.peashooter.visible = false;
                this.sunflower.visible = true;
                this.wallnut.visible = false;
                this.cherrybomb.visible = false;
                this.plant1_card.visible = false;
                this.plant2_card.visible = true;
                this.plant3_card.visible = false;
                this.plant4_card.visible = false;
            }
            else{
                this.plant2.loadTexture('plant2_black_check');
                game.time.events.add(Phaser.Timer.SECOND * 1, function(){
                    this.plant2.loadTexture('plant2_black');
                }, this);

            }
        }, this);
        this.plant3.events.onInputDown.add(function(){
            if(pass_level1){
                this.peashooter.visible = false;
                this.sunflower.visible = false;
                this.wallnut.visible = true;
                this.cherrybomb.visible = false;
                this.plant1_card.visible = false;
                this.plant2_card.visible = false;
                this.plant3_card.visible = true;
                this.plant4_card.visible = false;
            }
            else{
                this.plant3.loadTexture('plant3_black_check');
                game.time.events.add(Phaser.Timer.SECOND * 1, function(){
                    this.plant3.loadTexture('plant3_black');
                }, this);
            }
        }, this);
        this.plant4.events.onInputDown.add(function(){
            if(pass_level2){//新家的
                this.peashooter.visible = false;
                this.sunflower.visible = false;
                this.wallnut.visible = false;
                this.cherrybomb.visible = true;
                this.plant1_card.visible = false;
                this.plant2_card.visible = false;
                this.plant3_card.visible = false;
                this.plant4_card.visible = true;
            }//新家的
        }, this);

        this.peashooter = game.add.sprite(740, 140, 'Peashooter_s');
        this.peashooter.animations.add('swing',[0,1,2,3,4,5,6,7,8,9,10,11,12],0,false);
        this.peashooter.play('swing',12, true, true);
        this.peashooter.visible = false;

        this.sunflower = game.add.sprite(740, 140, 'sunflower_s');
        this.sunflower.animations.add('swing2',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],0,false);
        this.sunflower.play('swing2',12, true, true);
        this.sunflower.visible = false;

        this.wallnut = game.add.sprite(740, 140, 'wallnut_s');
        this.wallnut.animations.add('swing3',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],0,false);
        this.wallnut.play('swing3',14, true, true);
        this.wallnut.visible = false;

        this.cherrybomb = game.add.sprite(720, 140, 'CherryBomb_s');
        this.cherrybomb.animations.add('swing4',[0,1,2,3,4,5,6],0,false);
        this.cherrybomb.play('swing4',10, true, true);
        this.cherrybomb.visible = false;

    },
    update: function(){
        x = game.input.activePointer.positionDown.x;//點下去的位置
        y = game.input.activePointer.positionDown.y;
        console.log('x:'+x,'y:'+y);

    },


}

var zombiepageState={
    preload: function(){
        game.load.image('background','assets/almanac/Almanac_Zombie.jpg');
        game.load.spritesheet('Almanac_CloseButton','assets/almanac/Almanac_CloseButton.png',89,26);
        game.load.spritesheet('Almanac_IndexButton','assets/almanac/Almanac_IndexButton.png',164,26);
        game.load.image('zombie1','assets/almanac/zombie1.png');
        game.load.image('zombie2','assets/almanac/zombie2.png');
        game.load.image('zombie2_black','assets/almanac/zombie2_black.png');

        game.load.spritesheet('zombie1_s','assets/almanac/zombie1_s.png',166,144);
        game.load.spritesheet('zombie2_s','assets/almanac/zombie2_s.png',166,144);


        game.load.image('zombie1_card','assets/almanac/zombie1_card.png');
        game.load.image('zombie2_card','assets/almanac/zombie2_card.png');
    },
    create: function(){
        game.add.sprite(0, 0, 'background');
        game.add.button(850,567,'Almanac_CloseButton',function(){game.state.start('menu');},this, 1, 0,1,0)//over out down up
        game.add.button(60,567,'Almanac_IndexButton',function(){game.state.start('almanac');},this, 1, 0,1,0)//over out down up
        this.zombie1_card = game.add.sprite(610, 77, 'zombie1_card');
        this.zombie1_card.visible = false;
        this.zombie2_card = game.add.sprite(610, 77, 'zombie2_card');
        this.zombie2_card.visible = false;

        this.zombie1 = game.add.sprite(40, 100, 'zombie1');
        if(pass_level1) this.zombie2 = game.add.sprite(150, 100, 'zombie2');
        else this.zombie2 = game.add.sprite(150, 100, 'zombie2_black');
        this.zombie1.inputEnabled =true;
        this.zombie2.inputEnabled =true;

        this.zombie1.events.onInputDown.add(function(){
            this.zombie1_s.visible = true;
            this.zombie2_s.visible = false;
            this.zombie1_card.visible = true;
            this.zombie2_card.visible = false;

        }, this);
        this.zombie2.events.onInputDown.add(function(){
            if(pass_level1){
                this.zombie1_s.visible = false;
                this.zombie2_s.visible = true;
                this.zombie1_card.visible = false;
                this.zombie2_card.visible = true;
            }

        }, this);



        this.zombie1_s = game.add.sprite(670, 140, 'zombie1_s');
        this.zombie1_s.animations.add('swing',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21],0,false);
        this.zombie1_s.play('swing',6, true, true);
        this.zombie1_s.visible = false;

        this.zombie2_s = game.add.sprite(670, 140, 'zombie2_s');
        this.zombie2_s.animations.add('swing2',[0,1,2,3,4,5,6,7,8,9,10,11],0,false);
        this.zombie2_s.play('swing2',6, true, true);
        this.zombie2_s.visible = false;


    },
    update: function(){
        x = game.input.activePointer.positionDown.x;//點下去的位置
        y = game.input.activePointer.positionDown.y;
        //console.log('x:'+x,'y:'+y);

    },


}
