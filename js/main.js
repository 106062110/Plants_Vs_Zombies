var game = new Phaser.Game(1000, 600, Phaser.AUTO, 'canvas');
game.state.add('choose_level',choose_levelState);
//game.state.add('almanac',almanacState);

game.state.add('almanac',almanacState);
game.state.add('plantpage',plantpageState);
game.state.add('zombiepage',zombiepageState);

game.state.add('loading',loadingState);
game.state.add('choose_level',choose_levelState);
game.state.add('menu',menuState);
// game.state.start('loading');
//game.state.add('main', mainState);
game.state.add('level_1', level_1);
game.state.add('level_2', level_2);
game.state.start('loading');
