var level_2_bg;
var peashootersSTG2;
var sunflowersSTG2;
var shovelSTG2;
var nutsSTG2;
var carsSTG2;

var card_PeashooterSTG2;
var card_SunflowerSTG2;
var card_NutSTG2;


var invisiblePeashooterSTG2 = null;
var invisibleNutSTG2 = null;
var invisibleSunflowerSTG2 = null;
var scroll_front_finishedSTG2 = false;
var scroll_back_finishedSTG2 = false;

var auto_collect_sunSTG2 = false

var invisiblePeashooterPlacedSTG2 = false;
var invisibleSunflowerPlacedSTG2 = false;
var invisibleNutPlacedSTG2 = false;

var sunflowerCreateSunTimerSTG2 = 0;
var sunflowerCompleteSunTimerSTG2 = 0;
var peashooterLuanchTimerSTG2 = 0;
var sunflowerLuanchTimerSTG2 = 0;
var nutLuanchTimerSTG2 = 0;
var zombies_1sSTG2LuanchTimer = 0;
var peashooterAttackTimerSTG2 = 0;
var sunsumSTG2 = 0;
var sunTextSTG2;
var sunCollectorSTG2;
var sunLuanchTimerSTG2 = 0;
var gridSTG2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var firstSTG2 = true;
var normalKillNumSTG2 = 0;
var finalWaveKillNumSTG2 = 0;
var finalWave = false;
var pass_level2;
var detect_card_posSTG2 = false;
var winmusic_play_alreadySTG2 = true;
var cardSacleTimerSTG2 = 0;
var normalLaunchNumSTG2 = 0;
var disalbed = false;


var level_2 = {
    preload: function()
    {
        game.load.image("level_2_bg", "assets/Background/background1.jpg");
        game.load.image("sunBoard", "assets/interface/SunBack.png");
        game.load.image("peashooter_bullet", "assets/Plants/peashooter_bullet.png");
        game.load.image("Shovel", "assets/interface/Shovel.png");
        game.load.image("zombieWin", "assets/interface/ZombiesWon.png");
        game.load.image("level2_pass", "assets/Background/level2_pass_image.png");
        game.load.image("cherrybomb_card", "assets/cards_Illustration/card_cherrybomb.png");
        game.load.image("zombieWin", "assets/interface/ZombiesWon.png")
        game.load.spritesheet('illustration_button','assets/Background/illustration_button.png',235,66);
        game.load.spritesheet("zombie_1", "assets/Zombie/fuck1.png", 166, 126);
        game.load.spritesheet("car", "assets/interface/car.png",82, 70);
        game.load.spritesheet("peashooter", "assets/Plants/Repeater.png", 70, 70);
        game.load.spritesheet("sunflower", "assets/Plants/sunflower.png", 70, 70);
        game.load.spritesheet("nut", "assets/Plants/nut.png", 70, 70);
        game.load.spritesheet("sun", "assets/interface/sun.png",78, 78);
        game.load.spritesheet("flagZombie", "assets/Zombie/flagZ1.png", 144, 125);
        game.load.spritesheet("card_Peashooter","assets/card/Peashooter.png", 100, 60);
        game.load.spritesheet("card_Sunflower","assets/card/52.png", 100, 60);
        game.load.spritesheet("card_Nut","assets/card/85.png", 100, 60);
        game.load.audio('shovel_music', 'assets/music/shovel.mp3');
        game.load.audio('level_1_bgm', 'assets/music/level_1_bgm.mp3');
        game.load.audio('zombie_eat', 'assets/music/zombie_eat.mp3');
        game.load.audio('car_car_music', 'assets/music/car_car_music.mp3');
        game.load.audio('picksun_music', 'assets/music/picksun.mp3');
        game.load.audio('zombie_comeout_music', 'assets/music/zombie_comeout.mp3');
        game.load.audio('winmusic', 'assets/music/winmusic.mp3');
        game.load.audio('finalwave_music', 'assets/music/finalwave.mp3');
        game.load.audio('groan1_music', 'assets/music/groan1.mp3');
        game.load.audio('groan4_music', 'assets/music/groan4.mp3');
        game.load.audio('groan5_music', 'assets/music/groan5.mp3');
        game.load.spritesheet("status", "assets/interface/statusBar.png", 200, 50);


    },

    create: function()
    {
        level_2_bg;
        peashootersSTG2;
        sunflowersSTG2;
        shovelSTG2;
        nutsSTG2;
        carsSTG2;

        card_PeashooterSTG2;
        card_SunflowerSTG2;
        card_NutSTG2;


        invisiblePeashooterSTG2 = null;
        invisibleNutSTG2 = null;
        invisibleSunflowerSTG2 = null;
        scroll_front_finishedSTG2 = false;
        scroll_back_finishedSTG2 = false;

        auto_collect_sunSTG2 = false

        invisiblePeashooterPlacedSTG2 = false;
        invisibleSunflowerPlacedSTG2 = false;
        invisibleNutPlacedSTG2 = false;

        sunflowerCreateSunTimerSTG2 = 0;
        sunflowerCompleteSunTimerSTG2 = 0;
        peashooterLuanchTimerSTG2 = 0;
        sunflowerLuanchTimerSTG2 = 0;
        nutLuanchTimerSTG2 = 0;
        zombies_1sSTG2LuanchTimer = 0;
        peashooterAttackTimerSTG2 = 0;
        sunsumSTG2 = 0;
        sunTextSTG2;
        sunCollectorSTG2;
        sunLuanchTimerSTG2 = 0;
        gridSTG2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        firstSTG2 = true;
        normalKillNumSTG2 = 0;
        finalWaveKillNumSTG2 = 0;
        finalWave = false;
        pass_level2 = false;
        detect_card_posSTG2 = false;
        winmusic_play_alreadySTG2 = true;
        cardSacleTimerSTG2 = 0;
        normalLaunchNumSTG2 = 0;
        disalbed = false;
        winmusic_play_alreadySTG2 = true;

        //pass_level2 = true;
        music.pause();

        level_2_bg = game.add.tileSprite(0, 0, 1400, 600, 'level_2_bg');

        sunBoardSTG2 = game.add.sprite(200, 33, 'sunBoard');
        sunBoardSTG2.anchor.setTo(0.5, 0.5)
        sunBoardSTG2.inputEnabled = true;
        sunBoardSTG2.visible = false;

        sunTextSTG2 = game.add.text(225, 35, '', {fontSize: 50, fill: '#000'});
            sunTextSTG2.anchor.setTo(0.5, 0.5)
            sunTextSTG2.render = function (){
                sunTextSTG2.text = sunsumSTG2;
            };

        sunTextSTG2.visible = false;
        sunTextSTG2.render();

        sunCollectorSTG2 = game.add.sprite(125, 35, 'sun');
        game.physics.enable(sunCollectorSTG2, Phaser.Physics.ARCADE);
        sunCollectorSTG2.anchor.setTo(0.5, 0.5);
        sunCollectorSTG2.scale.setTo(0.5,0.5);
        sunCollectorSTG2.visible = false;

        peashootersSTG2 = game.add.group();
        peashootersSTG2.enableBody = true;
        peashootersSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        peashootersSTG2.setAll('checkWorldBounds', true);
        peashootersSTG2.setAll('outOfBoundsKill', true);
        peashootersSTG2.createMultiple(50, 'peashooter');
        peashootersSTG2.forEach(function(peashooterSTG2){
            peashooterSTG2.animations.add('swing', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 15, true);

            peashooterSTG2.anchor.setTo(0.5, 0.5);
            peashooterSTG2.inputEnabled = true;
            peashooterSTG2.input.enableDrag(true);
            peashooterSTG2.events.onDragStart.add(function(){
                peashooterSTG2.alpha = 0.2;
                peashooterSTG2.input.enableSnap(84, 100, false, true, 190, 105);
            }, this);

            peashooterSTG2.events.onDragStop.add(function(){
                setpeashooterposSTG2(peashooterSTG2);
            }, this);
        });

        statusBarSTG2 = game.add.sprite(800, 10, "status");
        statusBarSTG2.visible = false;


        sunflowersSTG2 = game.add.group();
        sunflowersSTG2.enableBody = true;
        sunflowersSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        sunflowersSTG2.setAll('checkWorldBounds', true);
        sunflowersSTG2.setAll('outOfBoundsKill', true);
        sunflowersSTG2.createMultiple(50, 'sunflower');
        sunflowersSTG2.forEach(function(sunflowerSTG2){
            sunflowerSTG2.animations.add('swing', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17], 18 , true);
            sunflowerSTG2.animations.add('makesun', [18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35], 18 , true);
            sunflowerSTG2.anchor.setTo(0.5, 0.5);
            sunflowerSTG2.inputEnabled = true;
            sunflowerSTG2.input.enableDrag(true);
            sunflowerSTG2.sunflowerCreateSunTimerSTG2 = 0
            sunflowerSTG2.sunflowerCreateSunSTG2 = false;
            sunflowerSTG2.events.onDragStart.add(function(){
                sunflowerSTG2.alpha = 0.2;
                sunflowerSTG2.input.enableSnap(84, 100, false, true, 190, 105);
            }, this);

            sunflowerSTG2.events.onDragStop.add(function(){
                setsunflowerposSTG2(sunflowerSTG2);
            }, this);
        });


        nutsSTG2 = game.add.group();
        nutsSTG2.enableBody = true;
        nutsSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        nutsSTG2.setAll('checkWorldBounds', true);
        nutsSTG2.setAll('outOfBoundsKill', true);
        nutsSTG2.createMultiple(50, 'nut');
        nutsSTG2.forEach(function(nutSTG2){
            nutSTG2.animations.add('nut_1', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 16 , true);
            nutSTG2.animations.add('nut_2', [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26], 11 , true);
            nutSTG2.animations.add('nut_3', [27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41], 15, true);
            nutSTG2.anchor.setTo(0.5, 0.5);
            nutSTG2.inputEnabled = true;
            nutSTG2.input.enableDrag(true);
            nutSTG2.events.onDragStart.add(function(){
                nutSTG2.alpha = 0.2;
                nutSTG2.input.enableSnap(84, 100, false, true, 190, 105);
            }, this);

            nutSTG2.events.onDragStop.add(function(){
                setnutposSTG2(nutSTG2);
            }, this);
        });

        zombie_1sSTG2 = game.add.group();
        zombie_1sSTG2.enableBody = true;
        zombie_1sSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        zombie_1sSTG2.setAll('checkWorldBounds', true);
        zombie_1sSTG2.setAll('outOfBoundsKill', true);
        zombie_1sSTG2.createMultiple(20, 'zombie_1');
        zombie_1sSTG2.forEach(function(zombie_1STG2){
            zombie_1STG2.animations.add('idle1', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 10, true);
            zombie_1STG2.animations.add('idle2', [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32], 10, true);
            zombie_1STG2.animations.add('idle3', [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43], 10, true);
            zombie_1STG2.animations.add('boom', [44, 45, 46, 47, 48, 49 , 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63], 10, true);
            zombie_1STG2.animations.add('walk', [64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85], 10, true);
            zombie_1STG2.animations.add('walk2', [86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116], 10, true);
            zombie_1STG2.animations.add('attack', [117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128 ,129, 130, 131, 132, 133, 134, 135, 136, 137], 10, true);
            zombie_1STG2.animations.add('walk_nohead', [148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165], 10, true);
            zombie_1STG2.animations.add('die', [138, 139, 140, 141, 142, 143, 144, 145, 146, 147], 10, true);
            zombie_1STG2.animations.add('attack_nohead', [166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176], 10, true);
            zombie_1STG2.anchor.setTo(0.5, 0.5);
        });
        luanchDemoZombiesSTG2();

        flagZombiesSTG2 = game.add.group();
        flagZombiesSTG2.enableBody = true;
        flagZombiesSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        flagZombiesSTG2.setAll('checkWorldBounds', true);
        flagZombiesSTG2.setAll('outOfBoundsKill', true);
        flagZombiesSTG2.createMultiple(20, 'flagZombie');
        flagZombiesSTG2.forEach(function(flagZombieSTG2){
            flagZombieSTG2.animations.add('idle1', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], 10, true);
            flagZombieSTG2.animations.add('walk', [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27], 8, true);
            flagZombieSTG2.animations.add('attack', [28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38], 8, true);
            flagZombieSTG2.animations.add('walk_nohead', [39, 40, 41, 42, 43, 44, 45, 46, 49, 50, 51, 52], 10, true);
            flagZombieSTG2.anchor.setTo(0.5, 0.5);
        });



        peashooter_bulletsSTG2 = game.add.group();
        peashooter_bulletsSTG2.enableBody = true;
        peashooter_bulletsSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        peashooter_bulletsSTG2.createMultiple(30, 'peashooter_bullet');
        peashooter_bulletsSTG2.setAll('outOfBoundsKill', true);
        peashooter_bulletsSTG2.setAll('checkWorldBounds', true);
        peashooter_bulletsSTG2.forEach(function(peashooter_bulletSTG2){
            peashooter_bulletSTG2.anchor.setTo(0, 0.5);
            peashooter_bulletSTG2.hitzombie = false
        });


        carsSTG2 = game.add.group();
        carsSTG2.enableBody = true;
        carsSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        carsSTG2.createMultiple(5, 'car');
        carsSTG2.setAll('outOfBoundsKill', true);
        carsSTG2.setAll('checkWorldBounds', true);
        carsSTG2.forEach(function(carSTG2){
            carSTG2.animations.add('car_animation', [0, 1], 2, true);
            carSTG2.anchor.setTo(0.5, 0.5);
            carSTG2.inputEnabled = true;
            carSTG2.events.onInputDown.add(function(carSTG2){
                // carSTG2.body.velocity.x = 200;
                pass_level2 = true;
            })
        });

        sunsSTG2 = game.add.group();
        sunsSTG2.enableBody = true;
        sunsSTG2.physicsBodyType = Phaser.Physics.ARCADE;
        sunsSTG2.createMultiple(20, 'sun');
        sunsSTG2.setAll('outOfBoundsKill', true);
        sunsSTG2.setAll('checkWorldBounds', true);
        sunsSTG2.forEach(function(sunSTG2){
            sunSTG2.animations.add('sun_glow', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], 22, true);
            sunSTG2.anchor.setTo(0.5, 0.5);
            sunSTG2.inputEnabled = true;
            sunSTG2.events.onInputDown.add(clickSunSTG2, sunSTG2, this);
        });

        card_PeashooterSTG2 = game.add.sprite(50, 30, 'card_Peashooter');
        card_PeashooterSTG2.anchor.setTo(0.5, 0.5)
        card_PeashooterSTG2.visible = false;
        card_PeashooterSTG2.animations.add('gradual', [0, 1, 2, 3, 4], 3, false);
        card_PeashooterSTG2.animations.play("gradual")
        card_PeashooterSTG2.frame = 4

        card_SunflowerSTG2 = game.add.sprite(50, 90, 'card_Sunflower');
        card_SunflowerSTG2.anchor.setTo(0.5, 0.5)
        card_SunflowerSTG2.visible = false;
        card_SunflowerSTG2.animations.add('gradual', [0, 1, 2, 3, 4], 3, false);
        card_SunflowerSTG2.animations.play("gradual")
        card_SunflowerSTG2.frame = 4

        card_NutSTG2 = game.add.sprite(50, 150, 'card_Nut');
        card_NutSTG2.anchor.setTo(0.5, 0.5)
        card_NutSTG2.visible = false;
        card_NutSTG2.animations.add('gradual', [0, 1, 2, 3, 4, 5, 6], 5, false);
        card_NutSTG2.frame = 6;

        shovelSTG2 = game.add.sprite(330, 30, 'Shovel');
        game.physics.enable(shovelSTG2, Phaser.Physics.ARCADE);
        shovelSTG2.anchor.setTo(0.5, 0.5);
        shovelSTG2.visible = false;
        shovelSTG2.inputEnabled = true;
        shovelSTG2.input.enableDrag(true);
        shovelSTG2.events.onDragStart.add(function(){
            shovelSTG2.input.enableSnap(84, 100, false, true, 190, 105);
        }, this);

        shovelSTG2.events.onDragStop.add(function(){
            destroyPlantDetectionSTG2(shovelSTG2);
            shovelSTG2.x = 340;
            shovelSTG2.y = 30;
        }, this);

        cherrybombCard = game.add.sprite(900, 500, "cherrybomb_card");
        game.physics.enable(cherrybombCard, Phaser.Physics.ARCADE);
        cherrybombCard.anchor.setTo(0.5, 0.5);
        cherrybombCard.visible = false;
        cherrybombCard.inputEnabled = true;
        cherrybombCard.enableBody = true;

        cherrybombCard.emitter = game.add.emitter(0, 0, 10)
        cherrybombCard.emitter.makeParticles('whitepoint');
        cherrybombCard.emitter.setScale(0, 2, 0, 2, 1200);
        cherrybombCard.emitter.gravity = 0;

        zombiesWon = game.add.sprite(500, 300, 'zombieWin');
        zombiesWon.anchor.setTo(0.5, 0.5)
        zombiesWon.inputEnabled = true;
        zombiesWon.visible = false;


        shovel_music = game.add.audio('shovel_music');
        shovel_music.loop = false;

        level_1_bgm = game.add.audio('level_1_bgm');
        level_1_bgm.loop = true;
        if(!muteflag) level_1_bgm.play();

        zombie_eat = game.add.audio('zombie_eat');
        zombie_eat.loop = false;

        car_car_music = game.add.audio('car_car_music');
        car_car_music.loop = false;

        picksun_music = game.add.audio('picksun_music');
        picksun_music.loop = false;

        zombie_comeout_music = game.add.audio('zombie_comeout_music');
        zombie_comeout_music.loop = false;

        winmusic = game.add.audio('winmusic');
        winmusic.loop = false;

        finalwave_music = game.add.audio('finalwave_music');
        finalwave_music.loop = false;

        finalWave_imgSTG2 = game.add.sprite(500, 300, 'FinalWave');
        finalWave_imgSTG2.anchor.setTo(0.5, 0.5)
        finalWave_imgSTG2.inputEnabled = true;
        finalWave_imgSTG2.visible = false;

        groan1_music = game.add.audio('groan1_music');
        groan1_music.loop = false;

        groan4_music = game.add.audio('groan4_music');
        groan4_music.loop = false;

        groan5_music = game.add.audio('groan5_music');
        groan5_music.loop = false;

        level2PassBG = game.add.sprite(0, 0, "level2_pass");
        level2PassBG.visible = false;

        back_buttonSTG2 = game.add.button(740,520, 'illustration_button', function(){
            music.resume();
            game.state.start('choose_level');

        },this, 0, 0,1,0);//over out down up
        back_buttonSTG2.anchor.setTo(0.5);
        back_buttonSTG2.visible = false;
    },

    update: function()
    {
        // sunTextSTG2.fill = "#FF0000"

        scroll_screenSTG2();
        if(sunsumSTG2 < 0){
            sunsumSTG2 = 0;
            sunTextSTG2.render()
        }
        if(scroll_back_finishedSTG2){
            enable_UISTG2();
            luanchSunsSTG2();
            luanchZombiesSTG2();

            sunflowerCreateSunSTG2();
            peashooter_attackSTG2();
            kill_bulletSTG2();
            car_car_car();
            game_over_detectSTG2();
            zombie_1sSTG2.forEachExists(function(zombieSTG2){
                zombieAttackSTG2(zombieSTG2);
                killZombie(zombieSTG2);
            });
            flagZombiesSTG2.forEachExists(function(flagZombieSTG2){
                zombieAttackSTG2(flagZombieSTG2);
            });
        }
        if(sunflag){
            autoCollectSunSTG2();
        }

        if(pass_level2){
            pass_level_2(cherrybombCard);
        }
        if(detect_card_posSTG2){
            play_Ani_cherrybombCard();
        }


        game.physics.arcade.overlap(sunsSTG2, sunCollectorSTG2, sunCollectedSTG2, null, this);
        game.physics.arcade.overlap(zombie_1sSTG2, carsSTG2, carhitZombiesSTG2, null, this);
        game.physics.arcade.overlap(flagZombiesSTG2, carsSTG2, carhitZombiesSTG2, null, this);
        // game.physics.arcade.overlap(flagZombiesSTG2, peashooter_bulletsSTG2, bulletHitFlagZombiesSTG2, null, this);
    }
};


function destroyPlantDetectionSTG2(shovelSTG2){
    var indexSTG2_shovel =  9 * ((shovelSTG2.y - 5 ) / 100 - 1 ) + (shovelSTG2.x - 274) / 84;
    console.log("shovelSTG2 index = " + indexSTG2_shovel);
    console.log((indexSTG2_shovel % 9) * 84 + 274);
    console.log(parseInt(indexSTG2_shovel / 9) * 100 + 105);
    if(gridSTG2[indexSTG2_shovel]){
        console.log("comein");
        peashootersSTG2.forEachExists(function(peashooterSTG2){
            if(peashooterSTG2.x == (indexSTG2_shovel % 9) * 84 + 274 &&  peashooterSTG2.y == parseInt(indexSTG2_shovel / 9) * 100 + 105){
                console.log("peashooterSTG2 kill");
                peashooterSTG2.kill();
                if(!muteflag) shovel_music.play();
                console.log("play shovel_music");
                gridSTG2[indexSTG2_shovel] = 0;
            }
        });
        sunflowersSTG2.forEachExists(function(sunflowerSTG2){
            if(sunflowerSTG2.x == (indexSTG2_shovel % 9) * 84 + 274 &&  sunflowerSTG2.y == parseInt(indexSTG2_shovel / 9) * 100 + 105){
                console.log("sunflowerSTG2 kill");
                sunflowerSTG2.kill();
                if(!muteflag) shovel_music.play();
                gridSTG2[indexSTG2_shovel] = 0;
            }
        });
        nutsSTG2.forEachExists(function(nutSTG2){
            if(nutSTG2.x == (indexSTG2_shovel % 9) * 84 + 274 &&  nutSTG2.y == parseInt(indexSTG2_shovel / 9) * 100 + 105){
                console.log("nutSTG2 kill");
                nutSTG2.kill();
                if(!muteflag) shovel_music.play();
                gridSTG2[indexSTG2_shovel] = 0;
            }
        });

    }
}



function game_over_detectSTG2(){
    zombie_1sSTG2.forEachExists(function(zombie_1STG2){
        if(zombie_1STG2.x <= 200){
            zombie_1STG2.y = 305;
            zombiesWon.visible = true;
            game.time.events.add(5000, function(){
                music.resume();
                level_1_bgm.destroy();
                pass_level2 = false;
                game.state.start('choose_level');
            }, this)
        }
    }, this);
}

function car_car_car(){
    carsSTG2.forEachExists(function(carSTG2){
        zombie_1sSTG2.forEachExists(function(zombie_1STG2){
            console.log("detect car_car_car");
            if(zombie_1STG2.x - carSTG2.x > -3 && zombie_1STG2.x - carSTG2.x < 3 &&  zombie_1STG2.y == carSTG2.y){
                zombie_1STG2.kill();
                if (statusBarSTG2.frame < 4){
                    normalKillNumSTG2 ++;
                    console.log(normalKillNumSTG2);
                }
                if (normalKillNumSTG2 == 3){
                    console.log(normalKillNumSTG2);
                    statusBarSTG2.frame ++;
                    normalKillNumSTG2 = 0;
                }
                if (finalWave)
                    finalWaveKillNumSTG2 ++;
                if (finalWaveKillNumSTG2 == 10 )
                    pass_level2 = true;

            }
        })
    })

    carsSTG2.forEachExists(function(carSTG2){
        flagZombiesSTG2.forEachExists(function(flagZombieSTG2){
            // console.log("detect car_car_car");
            if(flagZombieSTG2.x - carSTG2.x > -10 && flagZombieSTG2.x - carSTG2.x < 10 &&  flagZombieSTG2.y == carSTG2.y){
                flagZombieSTG2.kill();
            }
        })
    })
}



function peashooter_attackSTG2(){

    if(game.time.now > peashooterAttackTimerSTG2){
        peashootersSTG2.forEachExists(function(peashooterSTG2){
            if(peashooterSTG2.alpha != 1) return;

            zombie_1sSTG2.forEachExists(function(zombie_1STG2){
                if(zombie_1STG2.y ==  peashooterSTG2.y){
                    var peashooter_bulletSTG2 = peashooter_bulletsSTG2.getFirstExists(false);
                    if (peashooter_bulletSTG2) {
                        peashooter_bulletSTG2.reset(peashooterSTG2.x + 5, peashooterSTG2.y - 5);
                        console.log("peashooter_bulletSTG2 = " + peashooter_bulletSTG2.x + " " + peashooter_bulletSTG2.y);

                        peashooter_bulletSTG2.body.velocity.x = 200;

                    }
                }
            })

            flagZombiesSTG2.forEachExists(function(flagZombieSTG2){
                if(flagZombieSTG2.y ==  peashooterSTG2.y){
                    var peashooter_bulletSTG2 = peashooter_bulletsSTG2.getFirstExists(false);
                    if (peashooter_bulletSTG2) {
                        peashooter_bulletSTG2.reset(peashooterSTG2.x + 5, peashooterSTG2.y - 5);
                        console.log("peashooter_bulletSTG2 = " + peashooter_bulletSTG2.x + " " + peashooter_bulletSTG2.y);
                        peashooter_bulletSTG2.body.velocity.x = 200;

                    }
                }
            })

        });
        peashooterAttackTimerSTG2 = game.time.now + 3000;
    }
}

function killZombieSTG2(zombie){
    if (zombie.frame == 146){
        zombie.kill();
    }
}
function kill_bulletSTG2(){
    peashooter_bulletsSTG2.forEachExists(function(peashooter_bulletSTG2){
        zombie_1sSTG2.forEachExists(function(zombie_1STG2){
            if (zombie_1STG2.health > 0){
                console.log("detect collison");
                if(peashooter_bulletSTG2.x - zombie_1STG2.x >= -10 && peashooter_bulletSTG2.x - zombie_1STG2.x <= 10 && peashooter_bulletSTG2.y + 5 == zombie_1STG2.y){
                    peashooter_bulletSTG2.kill();
                    zombie_1STG2.health = zombie_1STG2.health - 10;
                    console.log("zombie_1STG2.health = " + zombie_1STG2.health);
                    if(zombie_1STG2.health <= 0){
                        zombie_1STG2.animations.play('die');
                        zombie_1STG2.body.velocity.x = 0;
                        if (statusBarSTG2.frame < 4){
                            normalKillNumSTG2 ++;
                            console.log(normalKillNumSTG2);
                        }
                        if (normalKillNumSTG2 == 3){
                            console.log(normalKillNumSTG2);
                            statusBarSTG2.frame ++;
                            normalKillNumSTG2 = 0;
                        }
                        if (finalWave)
                            finalWaveKillNumSTG2 ++;
                        if (finalWaveKillNumSTG2 == 10 )
                            pass_level2 = true;

                        //else normalKillNum ++;
                        //if (finalWaveKillNum == 3){
                            //pass_level1 = true;
                        //}

                        //edit
                        //if (statusBar.frame < 4){
                            //statusBar.frame ++;
                        //}
                    }
                }
            }
        })
    })

    peashooter_bulletsSTG2.forEachExists(function(peashooter_bulletSTG2){
        flagZombiesSTG2.forEachExists(function(flagZombieSTG2){
            // console.log("detect collison");
            if(peashooter_bulletSTG2.x - flagZombieSTG2.x >= -10 && peashooter_bulletSTG2.x - flagZombieSTG2.x <= 10 && peashooter_bulletSTG2.y + 5 == flagZombieSTG2.y){
                peashooter_bulletSTG2.kill();
                flagZombieSTG2.health = flagZombieSTG2.health - 10;
                console.log("flagZombieSTG2.health = " + flagZombieSTG2.health);
                if(flagZombieSTG2.health <= 0) flagZombieSTG2.kill();
            }
        })
    })

}

// function bulletHitFlagZombiesSTG2(flagZombiesSTG2, peashooter_bulletSTG2){
//     flagZombiesSTG2.health = flagZombiesSTG2.health - 10;
//     game.time.events.start(300)
//         peashooter_bulletSTG2.kill();
//         console.log("flagZombiesSTG2.health = " + flagZombiesSTG2.health);
//         if(flagZombiesSTG2.health <= 0) flagZombiesSTG2.kill();
// }

function sunflowerCreateSunSTG2(){
    sunflowersSTG2.forEachExists(function(sunflowerSTG2){
        if (sunflowerSTG2.alpha == 1){
            if(!sunflowerSTG2.sunflowerCreateSunSTG2) sunflowerSTG2.animations.play('swing')
            if(sunflowerSTG2.sunflowerCreateSunSTG2) sunflowerSTG2.animations.play('makesun')

            if (sunflowerSTG2.sunflowerCreateSunTimerSTG2 == 0) sunflowerSTG2.sunflowerCreateSunTimerSTG2 = game.time.now + 10000;
            if (game.time.now > sunflowerSTG2.sunflowerCreateSunTimerSTG2 && sunflowerSTG2.sunflowerCreateSunTimerSTG2 != 0){
                sunflowerSTG2.sunflowerCreateSunSTG2 = !sunflowerSTG2.sunflowerCreateSunSTG2
                var flowersunSTG2 = sunsSTG2.getFirstExists(false);
                if (!flowersunSTG2) return;
                flowersunSTG2.reset(sunflowerSTG2.x - 30, sunflowerSTG2.y - 30);
                sunflowerSTG2.sunflowerCreateSunTimerSTG2 = game.time.now + 10000;
            }

        }
    })

}

function carhitZombiesSTG2(zombie_1STG2, carSTG2){
    carSTG2.body.velocity.x = 200;
    if(!muteflag) car_car_music.play();
}

function autoCollectSunSTG2(){
    sunsSTG2.forEachExists(function(sunSTG2){
        if(!sunSTG2.body.velocity.y) clickSunSTG2(sunSTG2);
        else if (sunSTG2.y >= 300) clickSunSTG2(sunSTG2);
    });
}
function luanchSunsSTG2(){
    if(game.time.now > sunLuanchTimerSTG2){
        var sunSTG2 = sunsSTG2.getFirstExists(false);
        if (!sunSTG2) return;
        sunSTG2.reset(game.rnd.integerInRange(300,900), 0);
        sunSTG2.animations.play('sun_glow');
        sunSTG2.body.velocity.y = 50;
        sunLuanchTimerSTG2 = game.time.now + 7000;
    }
}

function clickSunSTG2(sunSTG2){
    if(sunSTG2.alpha == 0) return;
    sunSTG2.body.velocity.y = 0;
    game.physics.arcade.moveToXY(sunSTG2, 125, 35, 800);
    if(!muteflag) picksun_music.play();

}

function sunCollectedSTG2(sunCollectorSTG2, sunSTG2){
    sunSTG2.kill();
    sunsumSTG2 += 25;
    sunTextSTG2.render();
}

function enable_UISTG2(){

    card_PeashooterSTG2.visible = true;
    if(!invisiblePeashooterPlacedSTG2 && game.time.now > peashooterLuanchTimerSTG2){
        var invisiblePeashooterSTG2 = peashootersSTG2.getFirstExists(false);
        if (invisiblePeashooterSTG2) {
            invisiblePeashooterSTG2.reset(card_PeashooterSTG2.x, card_PeashooterSTG2.y);
            invisiblePeashooterSTG2.alpha = 0;
            invisiblePeashooterPlacedSTG2 = true;
            invisiblePeashooterSTG2.bringToTop();
        }
    }
    card_SunflowerSTG2.visible = true;
    if(!invisibleSunflowerPlacedSTG2 && game.time.now > sunflowerLuanchTimerSTG2){
        var invisibleSunflowerSTG2 = sunflowersSTG2.getFirstExists(false);
        if (invisibleSunflowerSTG2) {
            invisibleSunflowerSTG2.reset(card_SunflowerSTG2.x, card_SunflowerSTG2.y);
            invisibleSunflowerSTG2.alpha = 0;
            invisibleSunflowerPlacedSTG2 = true;
            invisibleSunflowerSTG2.bringToTop();
        }
    }

    card_NutSTG2.visible = true;
    if(!invisibleNutPlacedSTG2 && game.time.now > nutLuanchTimerSTG2){
        var invisibleNutSTG2 = nutsSTG2.getFirstExists(false);
        if (invisibleNutSTG2) {
            invisibleNutSTG2.reset(card_NutSTG2.x, card_NutSTG2.y);
            invisibleNutSTG2.alpha = 0;
            invisibleNutPlacedSTG2 = true;
            invisibleNutSTG2.bringToTop();
        }
    }

    sunBoardSTG2.visible = true;
    sunTextSTG2.visible = true;
    shovelSTG2.visible = true;
    statusBarSTG2.visible = true;
}


function control_zombies_pace_2STG2(){
    for(var i = 0; i < 5; i++) {
        var zombieSTG2 = zombie_1sSTG2.children[i];
        if (!zombieSTG2) return;
        zombieSTG2.x += 4;
    }
}

function control_zombies_pace_1STG2(){
    for(var i = 0; i < 5; i++) {
        var zombieSTG2 = zombie_1sSTG2.children[i];
        if (!zombieSTG2) return;
        zombieSTG2.x -= 4;
    }
}

function scroll_screenSTG2(){
    if(!scroll_front_finishedSTG2 && level_2_bg.tilePosition.x > -400){
        level_2_bg.tilePosition.x += -4;
        control_zombies_pace_1STG2();
        if(level_2_bg.tilePosition.x === -400){
            game.time.events.add(1000, function(){ scroll_front_finishedSTG2 = true;}, this);
        }
    }

    if(scroll_front_finishedSTG2 && level_2_bg.tilePosition.x < -20){
        level_2_bg.tilePosition.x += 4;
        control_zombies_pace_2STG2();
        if(level_2_bg.tilePosition.x == -20){
            scroll_back_finishedSTG2 = true;

            zombie_1sSTG2.forEachExists(function(zombie_1STG2){
                zombie_1STG2.kill();
            });
            if(!muteflag) zombie_comeout_music.play();
            for (var i = 0; i < 5; i++){
                var carSTG2 = carsSTG2.getFirstExists(false);
                 if (!carSTG2) return;
                 carSTG2.reset(180, 105 + 100*i);
                 carSTG2.body.velocity.x = 0;
            }
        }
    }
}


function setnutposSTG2(nutSTG2){

    console.log(nutSTG2.x + " " + nutSTG2.y);
    invisibleNutPlacedSTG2 = false;
    if(nutSTG2.x < 274  || nutSTG2.x > 946 || nutSTG2.y < 105 || nutSTG2.y > 505){
        nutSTG2.kill();
    }
    else{
        var indexSTG2 =  9 * ((nutSTG2.y - 5 ) / 100 - 1 ) + (nutSTG2.x - 274) / 84;
        if(gridSTG2[indexSTG2]){
            nutSTG2.kill();
        }
        else if(sunsumSTG2 < 100){
            nutSTG2.kill();
            sunTextSTG2.fill = "#FF0000"
            game.time.events.add(500, function(){
                sunTextSTG2.fill = "#000";
            }, this)
        }
        else{
            gridSTG2[indexSTG2] = 1;
            sunsumSTG2 -= 100;
            sunTextSTG2.render();
            nutSTG2.health = 200;
            nutSTG2.alpha = 1;
            nutSTG2.input.disableDrag();
            card_NutSTG2.animations.play("gradual")
            nutSTG2.animations.play("nut_1");
            nutLuanchTimerSTG2 = game.time.now + 2000;
        }
    }
}



function setpeashooterposSTG2(peashooterSTG2){

    console.log("setpeashooterposSTG2 = " + peashooterSTG2.x + " " + peashooterSTG2.y);
    invisiblePeashooterPlacedSTG2 = false;
    if(peashooterSTG2.x < 274  || peashooterSTG2.x > 946 || peashooterSTG2.y < 105 || peashooterSTG2.y > 505){
        peashooterSTG2.kill();
    }
    else{
        var indexSTG2 =  9 * ((peashooterSTG2.y - 5 ) / 100 - 1 ) + (peashooterSTG2.x - 274) / 84;

        if(gridSTG2[indexSTG2]) peashooterSTG2.kill();

        else if(sunsumSTG2 < 50){
            peashooterSTG2.kill();
            sunTextSTG2.fill = "#FF0000"
            game.time.events.add(500, function(){
                sunTextSTG2.fill = "#000";
            }, this)
        }

        else{
            gridSTG2[indexSTG2] = 1;
            sunsumSTG2 -= 50;
            sunTextSTG2.render();
            peashooterSTG2.health = 100;
            peashooterSTG2.alpha = 1;
            peashooterSTG2.input.disableDrag();

            peashooterSTG2.animations.play("swing");
            card_PeashooterSTG2.animations.play('gradual')
            // console.log("play gradual");
            peashooterLuanchTimerSTG2 = game.time.now + 2000;
        }
    }
}

function setsunflowerposSTG2(sunflowerSTG2){

    console.log(sunflowerSTG2.x + " " + sunflowerSTG2.y);
    invisibleSunflowerPlacedSTG2 = false;
    if(sunflowerSTG2.x < 274  || sunflowerSTG2.x > 946 || sunflowerSTG2.y < 105 || sunflowerSTG2.y > 505){
        sunflowerSTG2.kill();
    }
    else{
        var indexSTG2 =  9 * ((sunflowerSTG2.y - 5 ) / 100 - 1 ) + (sunflowerSTG2.x - 274) / 84;
        if(gridSTG2[indexSTG2]){
            sunflowerSTG2.kill();
        }
        else if(sunsumSTG2 < 50){
            sunflowerSTG2.kill();
            sunTextSTG2.fill = "#FF0000"
            game.time.events.add(500, function(){
                sunTextSTG2.fill = "#000";
            }, this)
        }
        else{
            gridSTG2[indexSTG2] = 1;
            sunsumSTG2 -= 100;
            sunTextSTG2.render();
            sunflowerSTG2.health = 100;
            sunflowerSTG2.alpha = 1;
            sunflowerSTG2.input.disableDrag();
            card_SunflowerSTG2.animations.play('gradual')
            sunflowerLuanchTimerSTG2 = game.time.now + 2000;
        }
    }
}


function luanchDemoZombiesSTG2(){
    for(var i = 0; i < 5; i++) {
        var zombie_1STG2 = zombie_1sSTG2.getFirstExists(false);
        if (!zombie_1STG2) return;
        zombie_1STG2.reset(game.rnd.integerInRange(1100,1250), 100 + 100*i);
        zombie_1STG2.animations.play('idle1');
    }
}

function luanchZombiesSTG2(){
    if (statusBarSTG2.frame < 4){
        if (!disalbed){
            if(game.time.now > zombies_1sSTG2LuanchTimer && !firstSTG2){
                var zombie_1STG2 = zombie_1sSTG2.getFirstExists(false);
                if (!zombie_1STG2) return;
                zombie_1STG2.reset(1000, Math.floor(Math.random()*5) * 100 + 105);
                console.log("zombie_1STG2 = " + zombie_1STG2.x + " " + zombie_1STG2.y);
                zombie_1STG2.animations.play('walk');
                zombie_1STG2.body.velocity.x = -50;
                zombies_1sSTG2LuanchTimer = game.time.now + 10000;
                zombie_1STG2.health = 100;
                zombie_1STG2.attackTimer = 0;
                normalLaunchNumSTG2 ++;
                if (normalLaunchNumSTG2 == 12){
                    disalbed = true;
                }
            }
            if (game.time.now > zombies_1sSTG2LuanchTimer && firstSTG2){
                var flagzombie_1STG2 = flagZombiesSTG2.getFirstExists(false);
                if (!flagzombie_1STG2) return;

                flagzombie_1STG2.reset(1000, 305);
                console.log("flagzombie_1STG2 = " + flagzombie_1STG2.x + " " + flagzombie_1STG2.y);
                flagzombie_1STG2.animations.play('walk');
                flagzombie_1STG2.body.velocity.x = -50;
                zombies_1sSTG2LuanchTimer = game.time.now + 10000;
                flagzombie_1STG2.health = 100;
                flagzombie_1STG2.attackTimer = 0;
                firstSTG2 = !firstSTG2;
            }
        }
    }
    else{
        if (!finalWave){
            finalWave_imgSTG2.visible = true;
            if(!muteflag)finalwave_music.play();
            game.time.events.add(2000, function(){
                finalWave_imgSTG2.visible = false;

            })
            for (var i = 0; i < 10; i ++){
                var zombie_1STG2 = zombie_1sSTG2.getFirstExists(false);
                if (!zombie_1STG2) return;
                zombie_1STG2.reset(1000 + 1*i, Math.floor(Math.random()*5) * 100 + 105);
                console.log("zombie_1STG2 = " + zombie_1STG2.x + " " + zombie_1STG2.y);
                zombie_1STG2.animations.play('walk');
                zombie_1STG2.body.velocity.x = -50;
                zombies_1sSTG2LuanchTimer = game.time.now + 6000;
                zombie_1STG2.health = 100;
                zombie_1STG2.attackTimer = 0;
                finalWave = true;
            }
        }
    }
}

function zombieAttackSTG2(zombieSTG2){
    var indexSTG2_zombles = 9 * ((zombieSTG2.y - 5 ) / 100 - 1 );
    if (zombieSTG2.x > 988){
        indexSTG2_zombles = 100;
    }

    else if(zombieSTG2.x > 232 && zombieSTG2.x <= 316){
        indexSTG2_zombles += 0
    }
    else if(zombieSTG2.x > 316 && zombieSTG2.x <= 400){
        indexSTG2_zombles += 1
    }
    else if(zombieSTG2.x > 400 && zombieSTG2.x <= 484){
        indexSTG2_zombles += 2
    }
    else if(zombieSTG2.x > 484 && zombieSTG2.x <= 568){
        indexSTG2_zombles += 3
    }
    else if(zombieSTG2.x > 568 && zombieSTG2.x <= 652){
        indexSTG2_zombles += 4
    }
    else if(zombieSTG2.x > 652 && zombieSTG2.x <= 736){
        indexSTG2_zombles += 5
    }
    else if(zombieSTG2.x > 736 && zombieSTG2.x <= 820){
        indexSTG2_zombles += 6
    }
    else if(zombieSTG2.x > 820 && zombieSTG2.x <= 904){
        indexSTG2_zombles += 7
    }
    else if(zombieSTG2.x > 904 && zombieSTG2.x < 988){
        indexSTG2_zombles += 8
    }


    if(gridSTG2[indexSTG2_zombles]) {
        if (zombieSTG2.health > 30){
            console.log("plant index:" + indexSTG2_zombles);
            zombieSTG2.animations.play('attack');
            zombieSTG2.body.velocity.x = 0;
            if (zombieSTG2.attackTimer < game.time.now){
                zombieSTG2.attackTimer = game.time.now + 1000;
                attackPlantSTG2(indexSTG2_zombles);
            }
        }
        if (zombieSTG2.health <= 30 && zombieSTG2.health > 0){
            zombieSTG2.animations.play('attack_nohead');
            zombieSTG2.body.velocity.x = 0;
            if (zombieSTG2.attackTimer < game.time.now){
                zombieSTG2.attackTimer = game.time.now + 2000;
                attackPlantSTG2(indexSTG2_zombles);
            }
        }


    }
    else{
        if (zombieSTG2.health <= 30 && zombieSTG2.health > 0){
            zombieSTG2.animations.play('walk_nohead');
            zombieSTG2.body.velocity.x = -12;
        }
        if (zombieSTG2.health > 30){
            zombieSTG2.animations.play('walk');
            zombieSTG2.body.velocity.x = -20;
        }
    }

}

function attackPlantSTG2(indexSTG2_zombles){
    peashootersSTG2.forEachExists(function(peashooterSTG2){
        if (peashooterSTG2.x == (indexSTG2_zombles % 9) * 84 + 274 &&  peashooterSTG2.y == parseInt(indexSTG2_zombles / 9) * 100 + 105){
            console.log("peashooterSTG2 being attack -> index : " + indexSTG2_zombles);
            peashooterSTG2.health -= 5;
            if(!muteflag) zombie_eat.play();
            if (peashooterSTG2.health <= 0){
                peashooterSTG2.kill();
                gridSTG2[indexSTG2_zombles] = 0;
            }
        }
    });

    sunflowersSTG2.forEachExists(function(sunflowerSTG2){
        if (sunflowerSTG2.x == (indexSTG2_zombles % 9) * 84 + 274 &&  sunflowerSTG2.y == parseInt(indexSTG2_zombles / 9) * 100 + 105){
            console.log("sunflowerSTG2 being attack -> index : " + indexSTG2_zombles);
            sunflowerSTG2.health -= 5;
            if(!muteflag) zombie_eat.play();
            if (sunflowerSTG2.health <= 0){
                sunflowerSTG2.kill();
                gridSTG2[indexSTG2_zombles] = 0;
            }
        }
    });

    nutsSTG2.forEachExists(function(nutSTG2){
        if (nutSTG2.x == (indexSTG2_zombles % 9) * 84 + 274 &&  nutSTG2.y == parseInt(indexSTG2_zombles / 9) * 100 + 105){
            console.log("nutSTG2 being attack -> index : " + indexSTG2_zombles);
            nutSTG2.health -= 5;
            if(!muteflag) zombie_eat.play();
            if(nutSTG2.health <= 200 && nutSTG2.health >= 120) nutSTG2.animations.play("nut_1");
            if(nutSTG2.health < 120 && nutSTG2.health >= 60) nutSTG2.animations.play("nut_2");
            if(nutSTG2.health < 60 && nutSTG2.health > 0) nutSTG2.animations.play("nut_3");
            if (nutSTG2.health <= 0){
                nutSTG2.kill();
                gridSTG2[indexSTG2_zombles] = 0;
            }
        }
    });
}

function pass_level_2(cherrybombCard){
    // pass_level1 = false;

    cherrybombCard.visible = true;
    cherrybombCard.inputEnabled = true;
    cherrybombCard.enableBody = true;
    cherrybombCard.events.onInputDown.add(function(){
        cherrybombCard.body.velocity.x = -200;
        cherrybombCard.body.velocity.y = -120;
        detect_card_posSTG2 = true;
    },this);
}


function play_Ani_cherrybombCard(){
    level_1_bgm.destroy();
    if(winmusic_play_alreadySTG2){
        if(!muteflag) winmusic.play();
        winmusic_play_alreadySTG2 = false;
    }

    if(cherrybombCard.x < 500){
        cherrybombCard.body.velocity.x = 0;
        cherrybombCard.body.velocity.y = 0;
        cherrybombCard.body.moves = false;

        if(game.time.now > cardSacleTimerSTG2){
            if(cherrybombCard.scale.x < 3){
                cherrybombCard.scale.x += 0.02;
                cherrybombCard.scale.y += 0.02;
            }
            else{
                detect_card_posSTG2 = false;
                cherrybombCard.emitter.x = cherrybombCard.x;
                cherrybombCard.emitter.y = cherrybombCard.y;
                cherrybombCard.emitter.start(true, 3000, 25, 15);
                //edit
                game.time.events.add(1000, function(){
                    level2PassBG.visible = true;
                    back_buttonSTG2.visible = true;
                    //level_1_bg_1 = game.add.tileSprite(0, 0, 1400, 600, 'level_1_bg_2');
                    //game.state.start('level_2');
                }, this);

            }
            cardSacleTimerSTG2 = game.time.now + 10;
        }
    }
}
