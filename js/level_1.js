var level_1_bg_1;
var sod_roll;
var sod_roll_cap;
var sod;
var cars;
var shovel;
var index;
var sunflowerCard;
var sunsum = 0;
var sunText;
var finalWave_img;
var sunCollector;
var peashooter;
var card_Peashooter;
var sol_flow_finish = false;
var scroll_front_finished = false;
var scroll_back_finished = false;
var sod_roll_finished = false;
var auto_collect_sun = false;
var winmusic_play_already = true;
var peashooterLuanchTimer = 0;
var zombies_1sLuanchTimer = 0;
var peashooterAttackTimer = 0;
var zombieAttackTimer = 0;
var sunLuanchTimer = 0;
var cardSacleTimer = 0;
var invisiblePeashooterPlaced = false;
let invisiblePeashooter = null;
var grid = [0, 0, 0, 0, 0, 0, 0, 0, 0];
var pass_level1;
var detect_card_pos = false;
//edit
var finalWaveKillNum = 0;
var normalKillNum = 0;
var normalLaunchNum = 0;
var zombieLaunchDisabled = false;
var finalWave = false;


var level_1 = {
    preload: function()
    {
        //edit
        game.load.image('sunflower','assets/Background/sunflower.png');
        game.load.spritesheet('illustration_button','assets/Background/illustration_button.png',235,66);
        game.load.image("level_1_bg_1", "assets/Background/background1unsodded.jpg");
        game.load.image("level_1_bg_2", "assets/Background/background1.jpg");
        game.load.image("sod_roll_cap", "assets/Background/SodRollCap.png");
        game.load.image("peashooter_bullet", "assets/Plants/peashooter_bullet.png");
        game.load.image("sod_roll", "assets/Background/SodRoll.png");
        game.load.image("whitepoint", "assets/interface/whitepoint.png");
        game.load.image("sunBoard", "assets/interface/SunBack.png");
        game.load.image("card_Peashooter", "assets/card/Peashooter_4.png");
        game.load.image("Shovel", "assets/interface/Shovel.png");
        game.load.image("FinalWave", "assets/interface/finalwave.png")
        game.load.image("sunflowerCard", "assets/cards_Illustration/card_sunflower.png");
        game.load.image("zombieWin", "assets/interface/ZombiesWon.png")
        game.load.spritesheet("sod", "assets/interface/sod_spritesheet.png", 725, 116);
        game.load.spritesheet("sun", "assets/interface/sun.png",78, 78);
        game.load.spritesheet("car", "assets/interface/car.png",82, 70);
        game.load.spritesheet("zombie_1", "assets/Zombie/zombie_sprite.png", 166, 144);
        game.load.spritesheet("peashooter", "assets/Plants/Repeater.png", 70, 70);
        game.load.spritesheet("status", "assets/interface/statusBar.png", 200, 50);
        game.load.audio('level_1_bgm', 'assets/music/level_1_bgm.mp3');
        game.load.audio('zombie_eat', 'assets/music/zombie_eat.mp3');
        game.load.audio('car_car_music', 'assets/music/car_car_music.mp3');
        game.load.audio('picksun_music', 'assets/music/picksun.mp3');
        game.load.audio('zombie_comeout_music', 'assets/music/zombie_comeout.mp3');
        game.load.audio('winmusic', 'assets/music/winmusic.mp3');
        game.load.audio('finalwave_music', 'assets/music/finalwave.mp3');
        game.load.audio('groan1_music', 'assets/music/groan1.mp3');
        game.load.audio('groan4_music', 'assets/music/groan4.mp3');
        game.load.audio('groan5_music', 'assets/music/groan5.mp3');
    },

    create: function()
    {
        sunsum = 0;
        music.pause();
        sol_flow_finish = false;
        scroll_front_finished = false;
        scroll_back_finished = false;
        sod_roll_finished = false;
        auto_collect_sun = false;
        peashooterLuanchTimer = 0;
        zombies_1sLuanchTimer = 0;
        peashooterAttackTimer = 0;
        zombieAttackTimer = 0;
        sunLuanchTimer = 0;
        cardSacleTimer = 0;
        invisiblePeashooterPlaced = false;
        invisiblePeashooter = null;
        grid = [0, 0, 0, 0, 0, 0, 0, 0, 0];
        pass_level1 = false;
        detect_card_pos = false;
        //edit
        finalWaveKillNum = 0;
        normalKillNum = 0;
        normalLaunchNum = 0;
        zombieLaunchDisabled = false;
        finalWave = false;

        level_1_bg_1 = game.add.tileSprite(0, 0, 1400, 600, 'level_1_bg_1');

        sunBoard = game.add.sprite(200, 33, 'sunBoard');
        sunBoard.anchor.setTo(0.5, 0.5)
        sunBoard.inputEnabled = true;
        sunBoard.visible = false;
        winmusic_play_already = true;

        sunText = game.add.text(225, 35, '', {fontSize: 50, fill: '#000'});
            sunText.anchor.setTo(0.5, 0.5)
            sunText.render = function (){
                sunText.text = sunsum;
            };

        sunText.visible = false;
        sunText.render();

        statusBar = game.add.sprite(800, 10, "status");
        statusBar.visible = false;


        sunCollector = game.add.sprite(125, 35, 'sun');
        game.physics.enable(sunCollector, Phaser.Physics.ARCADE);
        sunCollector.anchor.setTo(0.5, 0.5);
        sunCollector.scale.setTo(0.5,0.5);
        sunCollector.visible = false;


        sod = game.add.sprite(600, 320, "sod");
        sod.enableBody = true;
        sod.physicsBodyType = Phaser.Physics.ARCADE;
        sod.anchor.setTo(0.5, 0.5);
        sod.animations.add('sod_flow', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24], 12, false);

        card_Peashooter = game.add.sprite(50, 30, 'card_Peashooter');
        card_Peashooter.anchor.setTo(0.5, 0.5)
        //card_Peashooter.inputEnabled = true;
        card_Peashooter.visible = false;

        peashooters = game.add.group();
        peashooters.enableBody = true;
        peashooters.physicsBodyType = Phaser.Physics.ARCADE;
        peashooters.setAll('checkWorldBounds', true);
        peashooters.setAll('outOfBoundsKill', true);
        peashooters.createMultiple(50, 'peashooter');
        peashooters.forEach(function(peashooter){
            peashooter.animations.add('swing', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14], 15, true);
            peashooter.anchor.setTo(0.5, 0.5);
            peashooter.inputEnabled = true;
            peashooter.input.enableDrag(true);

            peashooter.events.onDragStart.add(function(){
                if(sunsum >= 50){
                    peashooter.alpha = 0.2;
                    peashooter.input.enableSnap(84, 72, false, true, 190, 84);
                }

            }, this);

            peashooter.events.onDragStop.add(function(){
                setpeashooterpos(peashooter);
                invisiblePeashooter.alpha = 1;
                invisiblePeashooter.input.disableDrag();
                invisiblePeashooterPlaced = false;
                invisiblePeashooter.animations.play("swing");
            }, this);

        });

        zombie_1s = game.add.group();
        zombie_1s.enableBody = true;
        zombie_1s.physicsBodyType = Phaser.Physics.ARCADE;
        zombie_1s.setAll('checkWorldBounds', true);
        zombie_1s.setAll('outOfBoundsKill', true);

        zombie_1s.createMultiple(10, 'zombie_1');
        zombie_1s.forEach(function(zombie_1){
            zombie_1.animations.add('idle1', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 10, true);
            zombie_1.animations.add('idle2', [11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32], 10, true);
            zombie_1.animations.add('idle3', [33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43], 10, true);
            zombie_1.animations.add('boom', [44, 45, 46, 47, 48, 49 , 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63], 10, true);
            zombie_1.animations.add('walk', [64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85], 10, true);
            zombie_1.animations.add('walk', [64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85], 10, true);
            zombie_1.animations.add('walk2', [86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116], 10, true);
            zombie_1.animations.add('attack', [117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128 ,129, 130, 131, 132, 133, 134, 135, 136, 137], 10, true);
            zombie_1.animations.add('walk_nohead', [148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165], 10, true);
            zombie_1.animations.add('die', [138, 139, 140, 141, 142, 143, 144, 145, 146, 147], 10, true);
            zombie_1.animations.add('attack_nohead', [166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176], 10, true);

            zombie_1.anchor.setTo(0.3, 0.7);
        });
        luanchDemoZombies();

        peashooter_bullets = game.add.group();
        peashooter_bullets.enableBody = true;
        peashooter_bullets.physicsBodyType = Phaser.Physics.ARCADE;
        peashooter_bullets.createMultiple(30, 'peashooter_bullet');
        peashooter_bullets.setAll('outOfBoundsKill', true);
        peashooter_bullets.setAll('checkWorldBounds', true);
        peashooter_bullets.forEach(function(peashooter_bullet){
            peashooter_bullet.anchor.setTo(0.5, 0.5);
        });


        suns = game.add.group();
        suns.enableBody = true;
        suns.physicsBodyType = Phaser.Physics.ARCADE;
        suns.createMultiple(20, 'sun');
        suns.setAll('outOfBoundsKill', true);
        suns.setAll('checkWorldBounds', true);
        suns.forEach(function(sun){
            sun.animations.add('sun_glow', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], 22, true);
            sun.anchor.setTo(0.5, 0.5);
            sun.inputEnabled = true;
            sun.events.onInputDown.add(clickSun, sun, this);
        });


        cars = game.add.group();
        cars.enableBody = true;
        cars.physicsBodyType = Phaser.Physics.ARCADE;
        cars.createMultiple(5, 'car');
        cars.setAll('outOfBoundsKill', true);
        cars.setAll('checkWorldBounds', true);
        cars.forEach(function(car){
            car.animations.add('car_animation', [0, 1], 2, true);
            car.anchor.setTo(0.5, 0.5);
            car.inputEnabled = true;
            car.events.onInputDown.add(function(){
                //car.body.velocity.x = 200;
            pass_level1 = true;
            },this);
        });

        shovel = game.add.sprite(330, 30, 'Shovel');
        game.physics.enable(shovel, Phaser.Physics.ARCADE);
        shovel.anchor.setTo(0.5, 0.5);
        shovel.visible = false;
        shovel.inputEnabled = true;
        shovel.input.enableDrag(true);
        shovel.events.onDragStart.add(function(){
            shovel.input.enableSnap(84, 72, false, true, 190, 84);
        }, this);
        shovel.visible = false;

        shovel.events.onDragStop.add(function(){
            destroyPlantDetection(shovel);
            shovel.x = 340;
            shovel.y = 30;
        }, this);

        sunflowerCard = game.add.sprite(900, 500, 'sunflowerCard');
        game.physics.enable(sunflowerCard, Phaser.Physics.ARCADE);
        sunflowerCard.anchor.setTo(0.5, 0.5);
        sunflowerCard.visible = false;
        sunflowerCard.inputEnabled = true;
        sunflowerCard.enableBody = true;

        sunflowerCard.emitter = game.add.emitter(0, 0, 10)
        sunflowerCard.emitter.makeParticles('whitepoint');
        sunflowerCard.emitter.setScale(0, 2, 0, 2, 1200);
        sunflowerCard.emitter.gravity = 0;

        finalWave_img = game.add.sprite(500, 300, 'FinalWave');
        finalWave_img.anchor.setTo(0.5, 0.5)
        finalWave_img.inputEnabled = true;
        finalWave_img.visible = false;

        zombiesWon = game.add.sprite(500, 300, 'zombieWin');
        zombiesWon.anchor.setTo(0.5, 0.5)
        zombiesWon.inputEnabled = true;
        zombiesWon.visible = false;

        game.input.mouse.capture = true;
        //edit
        level1PassBG = game.add.sprite(0, 0, 'sunflower');
        level1PassBG.visible = false;
        back_button = game.add.button(740,520, 'illustration_button', function(){
            music.resume();
            game.state.start('choose_level');
        },this, 0, 0,1,0);//over out down up
        back_button.anchor.setTo(0.5);
        back_button.visible = false;

        level_1_bgm = game.add.audio('level_1_bgm');
        level_1_bgm.loop = true;
        if(!muteflag) level_1_bgm.play();

        zombie_eat = game.add.audio('zombie_eat');
        zombie_eat.loop = false;

        car_car_music = game.add.audio('car_car_music');
        car_car_music.loop = false;

        picksun_music = game.add.audio('picksun_music');
        picksun_music.loop = false;

        zombie_comeout_music = game.add.audio('zombie_comeout_music');
        zombie_comeout_music.loop = false;

        winmusic = game.add.audio('winmusic');
        winmusic.loop = false;

        finalwave_music = game.add.audio('finalwave_music');
        finalwave_music.loop = false;

        groan1_music = game.add.audio('groan1_music');
        groan1_music.loop = false;

        groan4_music = game.add.audio('groan4_music');
        groan4_music.loop = false;

        groan5_music = game.add.audio('groan5_music');
        groan5_music.loop = false;


    },

    update: function()
    {
        scroll_screen();
        roll_sod();
        detect_gameover();
        if(sod_roll_finished){
            // set peashootercard and launch the peashooter
            enableUI();
            kill_bullet();
            // launch Zombies
            luanchZombies();
            luanchSuns();

            // addPeashooter();
            peashooter_attack();
            zombie_1s.forEachExists(function(zombie){
                zombieAttack(zombie);
                killZombie(zombie);
            })
        }

        if(pass_level1){
           pass_level_1(sunflowerCard);
       }

       if(detect_card_pos){
           play_Ani_sunflowerCard();
        }

        if(sunflag){
            autoCollectSun();
        }
        //temp

        // game.physics.arcade.overlap(zombie_1s, peashooter_bullets, bullethitZombies, null, this);
        game.physics.arcade.overlap(zombie_1s, cars, carhitZombies, null, this);
        game.physics.arcade.overlap(suns, sunCollector, sunCollected, null, this);
    }
};

function detect_gameover(){
    zombie_1s.forEachExists(function(zombie_1){
        if(zombie_1.x == 800 && !muteflag) groan1_music.play();

        if(zombie_1.x == 600 && !muteflag) groan4_music.play();

        if(zombie_1.x == 400 && !muteflag) groan5_music.play();

        if(zombie_1.x <= 100){
            level_1_bgm.destroy();
            music.resume();
            zombie_1s.forEachExists(function(zombie){
                console.log("done");
                zombie.body.velocity.x = 0;
            })
            suns.forEachExists(function(sun){
                sun.body.velocity.y = 0;
            })
            zombiesWon.visible = true;
            game.time.events.add(5000, function(){
                game.state.start('choose_level');

            })
        }
    })
}
function kill_bullet(){
    peashooter_bullets.forEachExists(function(peashooter_bullet){
        zombie_1s.forEachExists(function(zombie_1){
            if (zombie_1.health > 0){
                if(peashooter_bullet.x - zombie_1.x > -3 && peashooter_bullet.x - zombie_1.x < 3){
                    peashooter_bullet.kill();
                    zombie_1.health = zombie_1.health - 10;

                    console.log("zombie_1.health = " + zombie_1.health);
                    if(zombie_1.health <= 0){
                        zombie_1.animations.play('die');
                        zombie_1.body.velocity.x = 0;
                        if (finalWave) finalWaveKillNum ++;
                        else normalKillNum ++;
                        if (finalWaveKillNum == 3){
                            pass_level1 = true;
                        }

                        //edit
                        if (statusBar.frame < 4){
                            statusBar.frame ++;
                        }
                    }
                }
            }
        })
    });

}

function autoCollectSun(){
    suns.forEachExists(function(sun){
        if(sun.y > 300){
            clickSun(sun);
        }
    });
}

function play_Ani_sunflowerCard(){
    level_1_bgm.destroy();
    if(winmusic_play_already){
        if(!muteflag) winmusic.play();
        winmusic_play_already = false;
    }

    if(sunflowerCard.x < 500){
        sunflowerCard.body.velocity.x = 0;
        sunflowerCard.body.velocity.y = 0;
        sunflowerCard.body.moves = false;

        if(game.time.now > cardSacleTimer){
            if(sunflowerCard.scale.x < 3){
                sunflowerCard.scale.x += 0.02;
                sunflowerCard.scale.y += 0.02;
            }
            else{
                detect_card_pos = false;
                sunflowerCard.emitter.x = sunflowerCard.x;
                sunflowerCard.emitter.y = sunflowerCard.y;
                sunflowerCard.emitter.start(true, 3000, 25, 15);
                //edit
                game.time.events.add(1000, function(){
                    level1PassBG.visible = true;
                    back_button.visible = true;
                    //level_1_bg_1 = game.add.tileSprite(0, 0, 1400, 600, 'level_1_bg_2');
                    //game.state.start('level_2');
                }, this);

            }
            cardSacleTimer = game.time.now + 10;
        }
    }
}
function pass_level_1(sunflowerCard){
    // pass_level1 = false;

    sunflowerCard.visible = true;
    sunflowerCard.inputEnabled = true;
    sunflowerCard.enableBody = true;
    sunflowerCard.events.onInputDown.add(function(){
        sunflowerCard.body.velocity.x = -200;
        sunflowerCard.body.velocity.y = -120;
        detect_card_pos = true;
    },this);
}

function destroyPlantDetection(shovel){
    if(shovel.x >= 274  && shovel.x <= 946 && shovel.y == 300){
        index = (shovel.x - 274) / 84;
        if(grid[index]){
            peashooters.forEachExists(function(peashooter){
                if(peashooter.x == index * 84 + 274){
                    peashooter.kill();
                    grid[index] = 0;
                }
            });
        }
    }
}

function setpeashooterpos(peashooter){
    console.log(peashooter.x + " " + peashooter.y);
    if(peashooter.x < 274  || peashooter.x > 946 || peashooter.y != 300){
        peashooter.kill();
        console.log("peashooter kill");
    }
    else{
        var index = (peashooter.x - 274) / 84;
        if(grid[index] == 1) peashooter.kill();
        else{
            grid[index] = 1;
            sunsum -= 50;
            sunText.render();
            peashooterLuanchTimer = game.time.now + 2000;
            peashooter.health = 100;
        }

    }
}

function clickSun(sun){
    if(sun.alpha == 0) return;
    sun.body.velocity.y = 0;
    game.physics.arcade.moveToXY(sun, 125, 35, 800);
    if(!muteflag) picksun_music.play();

};

function luanchSuns(){
    if(game.time.now > sunLuanchTimer){
        var sun = suns.getFirstExists(false);
        if (!sun) return;
        sun.reset(game.rnd.integerInRange(300,900), 0);
        sun.animations.play('sun_glow');
        sun.body.velocity.y = 50;
        sunLuanchTimer = game.time.now + 10000;
    }
}

function peashooter_attack(){
    if(!zombie_1s.getFirstExists(true)) return;
    else{
        if(game.time.now > peashooterAttackTimer){
            peashooters.forEachExists(function(peashooter){
                if(peashooter.alpha != 1) return;
                var peashooter_bullet = peashooter_bullets.getFirstExists(false);
                if (peashooter_bullet) {
                    peashooter_bullet.reset(peashooter.x + 5, peashooter.y - 5);
                    peashooter_bullet.body.velocity.x = 200;
                }
            });
            peashooterAttackTimer = game.time.now + 3000;
        }
    }
}

function killZombie(zombie){
    if (zombie.frame == 146){
        zombie.kill();
    }
}


function carhitZombies(zombie_1, car){
    car.body.velocity.x = 200;
    if(!muteflag) car_car_music.play()
    if (finalWave) finalWaveKillNum ++;
    else normalKillNum ++;
    if (finalWaveKillNum == 3){
        pass_level1 = true;
        if(!muteflag) winmusic.play();
    }

    //edit
    if (statusBar.frame < 4){
        statusBar.frame ++;
    }
    zombie_1.kill();
}
//edit
function luanchZombies(){
    if(game.time.now > zombies_1sLuanchTimer){
        if (normalLaunchNum < 4){

            var zombie_1 = zombie_1s.getFirstExists(false);
            if (!zombie_1) return;
            zombie_1.reset(1050, 300);
            zombie_1.animations.play('walk');

            zombie_1.body.velocity.x = -12;
            zombies_1sLuanchTimer = game.time.now + 10000;
            zombie_1.health = 100;
            zombie_1.attackTimer = 0;
            normalLaunchNum ++;

        }
        else if (normalKillNum == 4){
            //edit
            if (!finalWave){
                finalWave_img.visible = true;
                if(!muteflag) finalwave_music.play();
                game.time.events.add(2000, function(){
                    finalWave_img.visible = false;

                })
                for(var i = 0; i < 3; i++){
                    var zombie_1 = zombie_1s.getFirstExists(false);
                    if (!zombie_1) return;
                    zombie_1.reset(1050-50*i, 300);
                    zombie_1.animations.play('walk');
                    zombie_1.body.velocity.x = -12;
                    zombie_1.health = 100;
                    zombie_1.attackTimer = 0;
                }
                finalWave = true;
                zombies_1sLuanchTimer = game.time.now + 5000;
            }
        }
    }



}

// launch demo zombies
function luanchDemoZombies(){
    for(var i = 0; i < 5; i++) {
        var zombie_1 = zombie_1s.getFirstExists(false);
        if (!zombie_1) return;
        zombie_1.reset(game.rnd.integerInRange(1100,1250), (i+1)*100 );
        zombie_1.animations.play('idle1');
    }
}

// control the position of zombies when background scrolling
function control_zombies_pace_1(){
    for(var i = 0; i < 5; i++) {
        var zombie = zombie_1s.children[i];
        if (!zombie) return;
        zombie.x -= 4;
    }
}
// control the position of zombies when background scrolling
function control_zombies_pace_2(){
    for(var i = 0; i < 5; i++) {
        var zombie = zombie_1s.children[i];
        if (!zombie) return;
        zombie.x += 4;
    }
}

function scroll_screen(){
    if(!scroll_front_finished && level_1_bg_1.tilePosition.x > -400){
        level_1_bg_1.tilePosition.x += -4;
        control_zombies_pace_1();
        //scroll the background front finished
        if(level_1_bg_1.tilePosition.x === -400){
            game.time.events.add(1000, function(){ scroll_front_finished = true;}, this);
        }
    }
    // scroll the background back
    if(scroll_front_finished && level_1_bg_1.tilePosition.x < -20){
        //game.state.start('level_2');
        level_1_bg_1.tilePosition.x += 4;
        control_zombies_pace_2();
        // scroll the background back finished
        if(level_1_bg_1.tilePosition.x === -20){
            scroll_back_finished = true;
            sod_roll = game.add.sprite(250, 315, 'sod_roll');
            sod_roll.anchor.setTo(0.5, 0.5);
            sod_roll_cap = game.add.sprite(248, 360, 'sod_roll_cap');
            sod_roll_cap.anchor.setTo(0.5, 0.5);
        }
    }
}

function roll_sod(){
    if( !sod_roll_finished && scroll_back_finished && sod_roll.x < 950){
         sod_roll.x += 5.8;
         sod_roll_cap.x += 5.8;
         if(!sol_flow_finish){
             sod.animations.play("sod_flow")
             sol_flow_finish = true;
         }

         //roll the sod finished
         if(sod_roll.x >= 950){
             game.time.events.add(100, function(){
                 sod_roll.kill();
                 sod_roll_cap.kill();
                 sod_roll_finished = true;


                 if(!muteflag) zombie_comeout_music.play();
                 var car = cars.getFirstExists(false);
                 if (!car) return;
                 car.reset(180, 300);
                 car.animations.play('car_animation');
                 car.body.velocity.x = 0;

                 // delete the demo zombies
                 for(var i = 0; i < 5; i++) {
                     var zombie = zombie_1s.children[i];
                     if (zombie)
                     zombie.kill();
                 }
             }, this);
         }
    }

}
function sunCollected(sunCollector, sun){
    console.log("done");
    sun.kill();
    sunsum += 25;
    sunText.render();
}

function enableUI(){
    card_Peashooter.visible = true;
    if(!invisiblePeashooterPlaced && game.time.now > peashooterLuanchTimer){
        invisiblePeashooter = peashooters.getFirstExists(false);
        if (invisiblePeashooter) {
            invisiblePeashooter.reset(card_Peashooter.x, card_Peashooter.y);
            invisiblePeashooter.alpha = 0;
            invisiblePeashooterPlaced = true;
            invisiblePeashooter.bringToTop();
        }
    }

    sunBoard.visible = true;
    sunText.visible = true;
    //shovel.visible = true;
    statusBar.visible = true;
    // console.log(card_Peashooter.z);
}

function zombieAttack(zombie){
    var index = Math.ceil((zombie.x - 274) / 84);
    console.log(index);
    if(grid[index] == 1) {
        if (zombie.health > 30){
            console.log('attack');
            zombie.animations.play('attack');

            zombie.body.velocity.x = 0;
            if (zombie.attackTimer < game.time.now){
                zombie.attackTimer = game.time.now + 2000;
                attackPlant(index);
            }
        }
        if (zombie.health <= 30 && zombie.health > 0){
            zombie.animations.play('attack_nohead');
            // zombie_eat.play();
            zombie.body.velocity.x = 0;
            if (zombie.attackTimer < game.time.now){
                zombie.attackTimer = game.time.now + 2000;
                attackPlant(index);
            }
        }

    }
    else{
        if (zombie.health <= 30 && zombie.health > 0){
            zombie.animations.play('walk_nohead');
            zombie.body.velocity.x = -12;
        }
        if (zombie.health > 30){
            zombie.animations.play('walk');
            zombie.body.velocity.x = -12;
        }

    }
}

function attackPlant(index){
    peashooters.forEachExists(function(peashooter){
        if (peashooter.x == index * 84 + 274){
            console.log(index);
            console.log("health = " + peashooter.health);
            peashooter.health -= 5;
            if(!muteflag) zombie_eat.play();
            if (peashooter.health <= 0){
                peashooter.kill();
                grid[index] = 0;
            }
        }
    });
}
