
var menuState={
    preload: function(){
        game.load.image('background','assets/menu/surface.png');
        game.load.spritesheet('btnstart','assets/menu/SelectorScreenStartAdventur.png',331,292/2);
        game.load.spritesheet('book','assets/menu/SelectorScreen_Almanac.png',99,99);
        game.load.image('Help','assets/menu/Help.png');
        game.load.image('Options','assets/menu/tomb.png');//選項圖
        game.load.image('nike','assets/menu/nike.png');//選項裡面的打勾
        game.load.image('whitenike','assets/menu/whitenike.png');
        game.load.spritesheet('OptionsBackButton','assets/menu/OptionsBackButton1.png',360,100);
    },
    create: function(){
        game.stage.backgroundColor = '#ffffff';
        game.add.sprite(0, 0, 'background');
        btnstart = game.add.button(530,100, 'btnstart', function(){}, this, 1, 0);//over out down up//開始冒險吧
        btnstart.scale.setTo(10/9,1);
        btnstart.events.onInputDown.add(function(){
            game.state.start('choose_level');
        })
        btnbook = game.add.button(425,425, 'book', function(){}, this, 1, 0);//over out down up//圖鑑
        //幫助
        Help = game.add.sprite(game.width/2, game.height/2, 'Help');
        Help.anchor.set(0.5);
        Help.visible = false;
        Help.inputEnabled =true;


        //選項
        Options = game.add.sprite(game.width/2, game.height/2, 'Options');
        Options.anchor.set(0.5);

        //打勾
        whitenike = game.add.sprite(432, 220, 'whitenike');
        whitenike.inputEnabled = true;
        whitenike.visible = false;

        nike = game.add.sprite(432, 220, 'nike');//432 220
        nike.inputEnabled =true;
        nike.visible = false;

        whitenike_2 = game.add.sprite(432, 246, 'whitenike');
        whitenike_2.inputEnabled = true;
        whitenike_2.visible = false;

        nike_2 = game.add.sprite(432, 246, 'nike');
        nike_2.inputEnabled =true;
        nike_2.visible = false;

        Options.visible = false;
        OptionsBackButton = game.add.button(505,488, 'OptionsBackButton', function(){
            Options.visible = false;
            OptionsBackButton.visible = false;
            if(nike.visible===true) this.sunflag=1;
            else this.sunflag=0;
            whitenike.visible = false;
            nike.visible = false;

            if(nike_2.visible===true) this.muteflag=1;
            else this.muteflag=0;
            whitenike_2.visible = false;
            nike_2.visible = false;
        },this, 0, 0,1,0);//over out down up//開始冒險吧


        OptionsBackButton.anchor.set(0.5);
        OptionsBackButton.visible = false;
        OptionsBackButton.inputEnabled =true;

        helparea = new Phaser.Rectangle(813, 518, 54, 32);//(左上X,左上Y,寬,高)
        optionarea = new Phaser.Rectangle(719, 489, 78, 28);//(左上X,左上Y,寬,高)
        exitarea = new Phaser.Rectangle(903, 517, 56, 20);//(左上X,左上Y,寬,高)
       // nikearea = new Phaser.Rectangle(432,220,15,15);
       //nikearea.inputEnabled =true;


    },
    update: function(){
        x = game.input.activePointer.positionDown.x;//點下去的位置
        y = game.input.activePointer.positionDown.y;
        console.log('x:'+x, 'y:'+y);
        if (helparea.contains(x,y)) Help.visible=true;

        if(Help.visible===true) {
            Help.events.onInputDown.add(function(){
                Help.visible = false;
            }, this);
        }

        if ( optionarea.contains(x,y) ) {
            Options.visible=true;
            OptionsBackButton.visible = true;
            if(this.sunflag===1){
                whitenike.visible = true;
                nike.visible = true;
            }
            else{
                whitenike.visible = true;
                nike.visible = false;
            }

            if(this.muteflag===1){
                whitenike_2.visible = true;
                nike_2.visible = true;
            }
            else{
                whitenike_2.visible = true;
                nike_2.visible = false;
            }
        }

        if(exitarea.contains(x,y)){
            game.state.start('loading');
        }

        if(whitenike.visible===true && nike.visible===false){
            whitenike.events.onInputDown.add(function(){
                whitenike.visible = false;
                nike.visible = true;
            }, this);
        }

        if(whitenike.visible===false && nike.visible===true){
            nike.events.onInputDown.add(function(){
                whitenike.visible = true;
                nike.visible = false;
            }, this);
        }

        if(whitenike_2.visible===true && nike_2.visible===false){
            whitenike_2.events.onInputDown.add(function(){
                whitenike_2.visible = false;
                nike_2.visible = true;
            }, this);
        }

        if(whitenike_2.visible===false && nike_2.visible===true){
            nike_2.events.onInputDown.add(function(){
                whitenike_2.visible = true;
                nike_2.visible = false;
            }, this);
        }


    },

}
